

-- creating DATABASE
CREATE DATABASE blog_db;


-- using blog_db database
USE blog_db;

CREATE TABLE users(

	id INT NOT NULL AUTO_INCREMENT,
	email VARCHAR(50) NOT NUll ,
	password VARCHAR(50) NOT NUll,
	datetime_created DATETIME NOT NULL,
	PRIMARY KEY (id)


);

CREATE TABLE posts(

	id INT NOT NULL AUTO_INCREMENT,
	author_id INT NOT NUll,
	content VARCHAR(5000) NOT NUll,
	datetime_posted DATETIME NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_posts_author_id
		FOREIGN KEY (author_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT


);




CREATE TABLE post_likes(

	id INT NOT NULL AUTO_INCREMENT,
	post_id INT NOT NUll ,
	user_id INT NOT NUll ,
	
	datetime_liked DATETIME NOT NULL,
	PRIMARY KEY (id),
		CONSTRAINT fk_post_likes_post_id
		FOREIGN KEY (post_id) REFERENCES posts(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
		CONSTRAINT fk_post_likes_user_id
		FOREIGN KEY (user_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT


);

CREATE TABLE post_comments(

	id INT NOT NULL AUTO_INCREMENT,
	post_id INT NOT NUll,
	user_id INT NOT NUll,
	content VARCHAR(5000) NOT NUll,
	datetime_commented DATETIME NOT NULL,
	
	datetime_liked DATETIME NOT NULL,
	PRIMARY KEY (id),
		CONSTRAINT fk_post_comments_post_id
		FOREIGN KEY (post_id) REFERENCES posts(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
		CONSTRAINT fk_post_comments_user_id
		FOREIGN KEY (user_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT


);

